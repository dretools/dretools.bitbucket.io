import os
from snakemake.utils import makedirs
ABSP = os.path.abspath("") + "/"


DATASETS = {
    "SRR3091828": 'scrRNA', 
    "SRR3091829": 'scrRNA', 
    "SRR3091830": 'scrRNA', 
    "SRR3091831": 'siRNA',
    "SRR3091832": 'siRNA',
    "SRR3091833": 'siRNA',
}

DATA = {
    'scrRNA':["SRR3091828", "SRR3091829", "SRR3091830"],  
    'siRNA': ["SRR3091831", "SRR3091832", "SRR3091833"]
}

DATASETS  = sorted([s for k,v in DATA.items() for s in v])
# ============================================================================
#                              Initial Files 
# ============================================================================

VCF_FILE     = "VCF/{sample}.ex.vcf"

# Note: .bai index files should also be present. 
BAM_FILE     = "BAM/{sample}.ex.bam"

# ============================================================================
#                              Files to Create
# ============================================================================

OUT_DIR  = "OUT/"
makedirs(ABSP + OUT_DIR)
MERGED_SITES = OUT_DIR + "consensus_sites.vcf"
ISLANDS      = OUT_DIR + "islands.bed"

EPK_DIR  = "EPK/"
makedirs(ABSP + EPK_DIR)
SAMPLE_EPK  = EPK_DIR + "{sample}.sample_epk.tsv"
ISLAND_EPK  = EPK_DIR + "{sample}.island_epk.tsv"
SITE_EPK    = EPK_DIR + "{sample}.edsite_epk.tsv"

# ============================================================================
#                                   ALL
# ============================================================================


rule all:
    input:
        expand(SAMPLE_EPK, sample=DATASETS),
        expand(SITE_EPK, sample=DATASETS),
        expand(ISLAND_EPK, sample=DATASETS),

# ============================================================================
#                                  Rules
# ============================================================================


rule merge_sites: 
    input: expand(VCF_FILE, sample=DATASETS)
    output: MERGED_SITES
    shell: """
    dretools edsite-merge \
        --min-editing 3   \
        --min-coverage 5  \
        --min-samples  3  \
        --vcf {input} > {output}
    """

rule find_islands:
    input: expand(VCF_FILE, sample=DATASETS)
    output: ISLANDS
    shell: """
    dretools find-islands \
        --min-editing 3   \
        --min-coverage 5  \
        --min-length 20   \
        --min-points 5    \
        --epsilon 50      \
       --vcf {input} > {output}
    """


rule find_sample_epk: 
    input: 
        consensus_sites = MERGED_SITES,
        bam = BAM_FILE
    output: SAMPLE_EPK
    shell: """dretools sample-epk --name {wildcards.sample} --vcf {input.consensus_sites} --alignment {input.bam} > {output} """


rule find_site_epk:
    input: 
        consensus_sites = MERGED_SITES,
        bam = BAM_FILE
    output: SITE_EPK
    shell: """dretools edsite-epk --vcf {input.consensus_sites} --alignment {input.bam} > {output} """


rule find_island_epk:
    input: 
        consensus_sites = MERGED_SITES,
        bam = BAM_FILE,
        regions = ISLANDS
    output: ISLAND_EPK
    shell: """dretools region-epk --vcf {input.consensus_sites} --regions {input.regions} --alignment {input.bam} > {output} """





