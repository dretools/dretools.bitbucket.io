#!/bin/bash

# Get editing in single end samples. 
snakemake --snakefile RNAEditor.single.sn.py --configfile data/config/single_config.json

# Get editing in paired end samples. 
snakemake --snakefile RNAEditor.paired.sn.py --configfile data/config/paired_config.json

# Calculate EPKs
snakemake --snakefile analyze_cells.sn.py    --configfile data/config/calc_epks.json

# =============================================
#        Find differentially edited islands. 
# =============================================

# siADAR vs scrRNA
dretools region-diff                                                                                        \
    --regions islands.bed                                                                                   \
    --min-depth 5                                                                                           \
    --names scrRNA,siRNA                                                                                    \
    --sample-epk EPK/SRR3091828.sample_EPK.tsv,EPK/SRR3091829.sample_EPK.tsv,EPK/SRR3091830.sample_EPK.tsv  \
                 EPK/SRR3091831.sample_EPK.tsv,EPK/SRR3091832.sample_EPK.tsv,EPK/SRR3091833.sample_EPK.tsv  \
    --region-epk EPK/SRR3091828.island_EPK.tsv,EPK/SRR3091829.island_EPK.tsv,EPK/SRR3091830.island_EPK.tsv  \
                 EPK/SRR3091831.island_EPK.tsv,EPK/SRR3091832.island_EPK.tsv,EPK/SRR3091833.island_EPK.tsv  \
    > huvec_diff_islands.tsv


# GM12787 vs K562
dretools region-diff  \
--names GM12787,K562  \
--regions islands.bed \
--region-epk          \
    EPK/SRR3192396.island_EPK.tsv,EPK/SRR3192397.island_EPK.tsv,EPK/SRR3192398.island_EPK.tsv,EPK/SRR3192399.island_EPK.tsv,EPK/SRR3192400.island_EPK.tsv,EPK/SRR3192401.island_EPK.tsv,EPK/SRR3192402.island_EPK.tsv,EPK/SRR3192403.island_EPK.tsv,EPK/SRR3192406.island_EPK.tsv,EPK/SRR3192407.island_EPK.tsv,EPK/SRR3192657.island_EPK.tsv,EPK/SRR3192658.island_EPK.tsv,EPK/SRR5048071.island_EPK.tsv,EPK/SRR5048072.island_EPK.tsv EPK/SRR3192408.island_EPK.tsv,EPK/SRR3192409.island_EPK.tsv,EPK/SRR3192410.island_EPK.tsv,EPK/SRR3192411.island_EPK.tsv,EPK/SRR3192412.island_EPK.tsv,EPK/SRR3192413.island_EPK.tsv,EPK/SRR3192414.island_EPK.tsv,EPK/SRR3192415.island_EPK.tsv,EPK/SRR3192416.island_EPK.tsv,EPK/SRR3192417.island_EPK.tsv,EPK/SRR3192422.island_EPK.tsv,EPK/SRR3192423.island_EPK.tsv,EPK/SRR3192544.island_EPK.tsv,EPK/SRR3192545.island_EPK.tsv \
--sample-epk \
    EPK/SRR3192396.sample_EPK.tsv,EPK/SRR3192397.sample_EPK.tsv,EPK/SRR3192398.sample_EPK.tsv,EPK/SRR3192399.sample_EPK.tsv,EPK/SRR3192400.sample_EPK.tsv,EPK/SRR3192401.sample_EPK.tsv,EPK/SRR3192402.sample_EPK.tsv,EPK/SRR3192403.sample_EPK.tsv,EPK/SRR3192406.sample_EPK.tsv,EPK/SRR3192407.sample_EPK.tsv,EPK/SRR3192657.sample_EPK.tsv,EPK/SRR3192658.sample_EPK.tsv,EPK/SRR5048071.sample_EPK.tsv,EPK/SRR5048072.sample_EPK.tsv EPK/SRR3192408.sample_EPK.tsv,EPK/SRR3192409.sample_EPK.tsv,EPK/SRR3192410.sample_EPK.tsv,EPK/SRR3192411.sample_EPK.tsv,EPK/SRR3192412.sample_EPK.tsv,EPK/SRR3192413.sample_EPK.tsv,EPK/SRR3192414.sample_EPK.tsv,EPK/SRR3192415.sample_EPK.tsv,EPK/SRR3192416.sample_EPK.tsv,EPK/SRR3192417.sample_EPK.tsv,EPK/SRR3192422.sample_EPK.tsv,EPK/SRR3192423.sample_EPK.tsv,EPK/SRR3192544.sample_EPK.tsv,EPK/SRR3192545.sample_EPK.tsv \
    > cells_diff_islands.tsv


# =============================================
#        Find differentially edited sites. 
# =============================================

# siADAR vs scrRNA
dretools edsite-diff                                                                               \
    --min-depth 5                                                                                  \
    --names scrRNA,siRNA                                                                           \
    --sample-epk                                                                                   \
        EPK/SRR3091828.sample_EPK.tsv,EPK/SRR3091829.sample_EPK.tsv,EPK/SRR3091830.sample_EPK.tsv  \
        EPK/SRR3091831.sample_EPK.tsv,EPK/SRR3091832.sample_EPK.tsv,EPK/SRR3091833.sample_EPK.tsv  \
    --site-epk                                                                                     \
        EPK/SRR3091828.site_EPK.tsv,EPK/SRR3091829.site_EPK.tsv,EPK/SRR3091830.site_EPK.tsv        \
        EPK/SRR3091831.site_EPK.tsv,EPK/SRR3091832.site_EPK.tsv,EPK/SRR3091833.site_EPK.tsv        \
    > huvec_diff_sites.tsv


# GM12787 vs K562
dretools edsite-diff     \
    --names GM12787,K562 \
    --vcf EPK/SRR3192396.site_EPK.tsv,EPK/SRR3192397.site_EPK.tsv,EPK/SRR3192398.site_EPK.tsv,EPK/SRR3192399.site_EPK.tsv,EPK/SRR3192400.site_EPK.tsv,EPK/SRR3192401.site_EPK.tsv,EPK/SRR3192402.site_EPK.tsv,EPK/SRR3192403.site_EPK.tsv,EPK/SRR3192406.site_EPK.tsv,EPK/SRR3192407.site_EPK.tsv,EPK/SRR3192657.site_EPK.tsv,EPK/SRR3192658.site_EPK.tsv,EPK/SRR5048071.site_EPK.tsv,EPK/SRR5048072.site_EPK.tsv EPK/SRR3192408.site_EPK.tsv,EPK/SRR3192409.site_EPK.tsv,EPK/SRR3192410.site_EPK.tsv,EPK/SRR3192411.site_EPK.tsv,EPK/SRR3192412.site_EPK.tsv,EPK/SRR3192413.site_EPK.tsv,EPK/SRR3192414.site_EPK.tsv,EPK/SRR3192415.site_EPK.tsv,EPK/SRR3192416.site_EPK.tsv,EPK/SRR3192417.site_EPK.tsv,EPK/SRR3192422.site_EPK.tsv,EPK/SRR3192423.site_EPK.tsv,EPK/SRR3192544.site_EPK.tsv,EPK/SRR3192545.site_EPK.tsv \
    --sample-epk EPK/SRR3192396.sample_EPK.tsv,EPK/SRR3192397.sample_EPK.tsv,EPK/SRR3192398.sample_EPK.tsv,EPK/SRR3192399.sample_EPK.tsv,EPK/SRR3192400.sample_EPK.tsv,EPK/SRR3192401.sample_EPK.tsv,EPK/SRR3192402.sample_EPK.tsv,EPK/SRR3192403.sample_EPK.tsv,EPK/SRR3192406.sample_EPK.tsv,EPK/SRR3192407.sample_EPK.tsv,EPK/SRR3192657.sample_EPK.tsv,EPK/SRR3192658.sample_EPK.tsv,EPK/SRR5048071.sample_EPK.tsv,EPK/SRR5048072.sample_EPK.tsv EPK/SRR3192408.sample_EPK.tsv,EPK/SRR3192409.sample_EPK.tsv,EPK/SRR3192410.sample_EPK.tsv,EPK/SRR3192411.sample_EPK.tsv,EPK/SRR3192412.sample_EPK.tsv,EPK/SRR3192413.sample_EPK.tsv,EPK/SRR3192414.sample_EPK.tsv,EPK/SRR3192415.sample_EPK.tsv,EPK/SRR3192416.sample_EPK.tsv,EPK/SRR3192417.sample_EPK.tsv,EPK/SRR3192422.sample_EPK.tsv,EPK/SRR3192423.sample_EPK.tsv,EPK/SRR3192544.sample_EPK.tsv,EPK/SRR3192545.sample_EPK.tsv \
> cells_diff_sites.tsv




