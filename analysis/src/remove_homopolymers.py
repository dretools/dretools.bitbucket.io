from pysam import Fastafile

# variants, outFile, distance
def remove_homopolymers(vcf_file_name, reference_genome_fasta_file, distance):
    distance = int(distance)
    # Create a Pysam fasta file object
    fasta_obj = Fastafile(reference_genome_fasta_file)
    
    for variant_line in open(vcf_file_name):
        if len(variant_line) > 0 and variant_line[0] == "#":
            print(variant_line.strip())
        else:    
            variant_chr, var_pos, var_id, var_ref, var_alt, var_qual, var_filter, var_info, var_format, var_A = variant_line.split()
            
            # FASTA files are also zero based. So subtract one. 
            var_pos = int(var_pos) - 1

            start_pos = var_pos - distance if var_pos >= distance else 0
            end_pos = var_pos + distance
            pattern = var_ref * distance
            
            # Fasta sequences are zero based 
            if pattern not in fasta_obj.fetch(variant_chr, start_pos, end_pos):
                print(variant_line.strip())
        

if __name__ == "__main__":
    import argparse    

    parser = argparse.ArgumentParser(prog='remove_edge_mismatches')

    parser.add_argument('--vcf',
                        dest='vcf',
                        type=str,
                        required=True, 
                        help="A vcf file.")

    parser.add_argument('--reference-genome',
                        dest='reference_genome',
                        type=str,
                        required=True, 
                        help="A reference genome fasta file.")

    parser.add_argument('--distance',
                        type=int,
                        default=4,
                        #nargs=1,
                        help='The distance to check for homopolymers.')

    args = parser.parse_args()
        
    remove_homopolymers(args.vcf, args.reference_genome, args.distance)






