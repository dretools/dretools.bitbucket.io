from pysam import Samfile


def generate_fasta_from_vcf_and_bam(variant_file, bam_file, min_base_quality, min_mismatch):
    """
    :param self:
    :param variant_file:
    :param bam_file:
    :param out_file:
    :param min_base_quality:
    :param min_mismatch:
    :return:
    """
    bam_file = Samfile(bam_file, "rb")

    # create Fasta file for blat to remap the variant overlapping reads
    for variant_line in open(variant_file):
        if len(variant_line) > 0 and variant_line[0] == "#":
            # print(variant_line.strip())
            pass
        else:
            var_info = variant_line.split("\t")
            var_chr = var_info[0]
            
            # Subtract one from the VCF coordinate to convert to a zero based coordinate.
            # BAM files are also zero based. 
            var_pos_m1 = int(var_info[1]) -1
            var_pos    = int(var_info[1]) 

            var_ref = var_info[3]
            var_alt = var_info[4]

            # Ignore reads
            pileup_iter = bam_file.pileup(var_chr, var_pos_m1, var_pos)
            alignments = []
            for x in pileup_iter:
                if x.pos == var_pos_m1:
                    # loop over reads of that position
                    for pileup_read in x.pileups:
                        if not pileup_read.is_del and not pileup_read.is_refskip:
                            if (
                               pileup_read.alignment.query_sequence[pileup_read.query_position] == var_alt and
                               pileup_read.alignment.query_qualities[pileup_read.query_position] >= min_base_quality):
                                alignments.append(pileup_read.alignment.seq)
            
            if len(alignments) >= min_mismatch:
                mismatch_read_count = 0
                for sequence in alignments:
                    print(">%s-%s-%s-%s-%s\n%s" % (var_chr, var_pos, var_ref, var_alt, mismatch_read_count, sequence,))
                    mismatch_read_count += 1


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(prog='remove_edge_mismatches')

    parser.add_argument('--vcf',
                        type=str,
                        required=True,
                        help="A vcf file.")

    parser.add_argument('--bam',
                        type=str,
                        required=True,
                        help="A reference genome fasta file.")

    parser.add_argument('--min-base-qual',
                        type=int,
                        default=25,
                        help="")

    parser.add_argument('--min-mismatch',
                        type=int,
                        default=2,
                        help="")

    args = parser.parse_args()

    generate_fasta_from_vcf_and_bam(args.vcf, args.bam, args.min_base_qual, args.min_mismatch)
