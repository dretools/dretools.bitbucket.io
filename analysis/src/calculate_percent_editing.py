import sys

out_list = []
for line in open(sys.argv[1]):

    split_line = line.strip().split()

    e_cnt  = float(split_line[-2])  
    ei_len = float(split_line[-1])
    pe = round(100.0 * e_cnt/ei_len, 2)

    split_line.append(str(pe))

    print("\t".join(split_line))
        

