import os
from snakemake.utils import makedirs
ABSP = os.path.abspath("") + "/"

MIN_EDITING  = int(config["min_editing"])
MIN_COVERAGE = int(config["min_coverage"])

# ============================================================================
#                              Initial Files 
# ============================================================================

MERGED_SITES = config["merged_sites"]
ISLANDS      = config["islands"]
VCF_FILE     = "PURIFICATION/{sample}.all_editing_sites.vcf"
BAM_DIR      = "PURIFICATION/"
BAM_FILE     = BAM_DIR + "{sample}.noDup.realigned.recalibrated.bam"

# ============================================================================
#                              Files to Create
# ============================================================================

# Bai files should already be avalible, but if not create them. 
BAI_FILE = BAM_DIR + "{sample}.noDup.realigned.recalibrated.bam.bai"

MAIN_STATS_DIR = "STATS/"
CUTOFF_DIR     = "ME%sMC%s/" % (MIN_EDITING, MIN_COVERAGE)

EPK_DIR  = "EPK/"
makedirs(ABSP + EPK_DIR)
SAMPLE_EPK  = EPK_DIR + "{sample}.sample_EPK.tsv"
ISLAND_EPK  = EPK_DIR + "{sample}.island_EPK.tsv"
SITE_EPK    = EPK_DIR + "{sample}.edsite_EPK.tsv"

STATS_DIR = MAIN_STATS_DIR + CUTOFF_DIR
makedirs(ABSP + STATS_DIR)
SAMPLE_STATS_FILE = STATS_DIR + "{sample}.sample.stats"

# ============================================================================
#                                   ALL
# ============================================================================

DATASETS = [line.strip() for line in open(config["samples"])]

rule all:
    input:
        expand(SAMPLE_STATS_FILE, sample=DATASETS),
        expand(SITE_EPK, sample=DATASETS)

# ============================================================================
#                                  Rules
# ============================================================================

rule make_index:
    input: BAM_FILE
    output: BAI_FILE
    shell: """ samtools index {input} {output} """


rule find_sample_epk: 
    input: BAM_FILE
    params: merged_editing_sites = MERGED_SITES
    output: SAMPLE_EPK
    shell: """dretools sample-epk --name {wildcards.sample} --vcf {params.merged_editing_sites} --alignment {input} > {output} """


rule find_site_epk:
    input: BAM_FILE
    params: merged_editing_sites = MERGED_SITES,
    output: SITE_EPK
    shell: """dretools edsite-epk --vcf {params.merged_editing_sites} --alignment {input} > {output} """


rule find_island_epk:
    input: BAM_FILE
    params:
        merged_editing_sites = MERGED_SITES,
        regions = ISLANDS
    output: ISLAND_EPK
    shell: """dretools region-epk --vcf {params.merged_editing_sites} --regions {params.regions} --alignment {input} > {output} """


rule find_sample_stats:
    input:
         vcf = VCF_FILE, 
         cov = COVERAGE_FILES
    params: 
        min_editing  = MIN_EDITING,
        min_coverage = MIN_COVERAGE
    output: SAMPLE_STATS_FILE
    shell: """
    dretools sample-stats                   \
        --names        {wildcards.sample}   \
        --min-editing  {params.min_editing} \
        --vcf          {input.vcf}          \
        --coverage     {input.cov}          \
        > {output} 
    """


