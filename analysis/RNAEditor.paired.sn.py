"""
RNAEditorSM - RNAEditor SnakeMake Edition
==============================================================================

Table of contents: 
- Introduction
- Installation 
- Running RNAEditorSM

== Introduction ==

This is an implmentation of the RNAEditor pipeline 
(https://doi.org/10.1093/bib/bbw087). In the original manuscript we published 
a GUI for finding RNA Editing sites. However, we have seen an increasing need
for an implementation able to run on HPC envioroments. RNAEditorSM is our 
solution to this. Implementation in Snakemake allows for automatic paralization 
across HPC clusters. 

== Installation ==

Required Software:

    Install following tools to /usr/local/bin/:

        BWA
        Picard Tools Move all .jar files to /usr/local/bin/picard-tools/ (use version <= 1.119)
        GATK Use GATK version 3.5 with java 1.7, the newer versions are currently causing problems.
        Blat
        Bedtools
        vectools (python3 -m pip install vectools)

    Required python packages:
        python3 -m pip install pysam       
        

== Running RNAEditorSM == 
To run RNAEditorSM two files are needed and depending on the HPC enviroment
it is likely that a third file is needed. Finally note that RNAEditorSM is 
only designed to work on paired end libararies. 

The two basic files are a config file are: 
- A config file. This file describes the locations of the programs and 
  files needed for the pipeline. 
- A sample file. This file describes the samples to run through the pipeline.
- (Optinal) a cluter config file. This file describes the HPC resources. We 
  give an example of this file. However, due to the variation in HPC
  enviroments we highly recomend consulting with your local HPC administrator 
  and the Snakemake documentation. 

Example config file (config.json). This file should be a json file with all the entries customized to your local 
system. 
{
    "samples": "samples.txt",
    "stand_call": "1",
    "stand_emit": "1",
    "threads": 1,
    "max_diff":  "0.04",
    "seed_diff": "2",
    "distance":  "4",
    "SNPs": "/media/tyler/DATA/Storage/references/9606/e83/dbSNP.vcf",
    "editing_types": "/media/tyler/DATA/Storage/references/9606/e83/editing_types_from_base_to_base.txt",
    "reference_genome": "/media/tyler/DATA/Storage/references/9606/e83/Homo_sapiens.GRCh38.dna.primary_assembly.fa",
    "repeats": "/media/tyler/DATA/Storage/references/9606/e83/repeats.bed",
    "gene_names": "/media/tyler/DATA/Storage/references/9606/e83/GRCh38.83.gtf.bed",
    "gtf": "/media/tyler/DATA/Storage/references/9606/e83/Homo_sapiens.GRCh38.83.gtf",
    "chromosomes": "/media/tyler/DATA/Storage/references/9606/e83/Homo_sapiens.GRCh38.83.chromosomes"
    "gatk_path": "/usr/local/bin/GATK_3.7-0-gcfedb67/GenomeAnalysisTK.jar",
}

Example sample file (samples.txt)
samples.txt - Should contain one file ID per line. 
Ex: 
SRR5575725
SRR787276
TODO: Remember to finx is not None in vectools
 
"""
from snakemake.utils import makedirs
from os.path import abspath

ABSOLUTE_PATH = os.path.abspath("") + "/"
TMP_PATH = ABSOLUTE_PATH + "EDITING_TMP"
makedirs(TMP_PATH)

REPEATS          = config["repeats"]
DISTANCE         = config["distance"]
SNPS             = config["SNPs"]
STAND_CALL       = config["stand_call"]
STAND_EMIT       = config["stand_emit"]
THREADS          = config["threads"]
GATK_PATH        = config["gatk_path"]
PICARD_PATH      = config["picard_path"]
MAX_DIFF         = config["max_diff"]
SEED_DIFF        = config["seed_diff"]
REFERENCE_GENOME = config["reference_genome"]
GENOME_LENGTHS   = config["genome_lengths"]
EXON_BED         = config["exons"] 
SPLICE_JUNCTIONS = config["splice_junctions"] 

try:
    THREADS = int(THREADS)
except ValueError:
    exit("Error: Threads must be an integer.")

# ============================================================================
#                             Assign Varibles
# ============================================================================


SRA_DIR = "SRA/"
makedirs(ABSOLUTE_PATH + SRA_DIR)
SRA_FILE = SRA_DIR + "{sample}.sra"

FASTQ_DIR = "FASTQS/"
makedirs(ABSOLUTE_PATH + FASTQ_DIR)
FASTQ_1 = FASTQ_DIR + "{sample}_1.fastq"
FASTQ_2 = FASTQ_DIR + "{sample}_2.fastq"

FASTQS = "TRIMMED_FASTQS/"
makedirs(ABSOLUTE_PATH + FASTQS)
FASTQ_FILE_1     = FASTQS + "{sample}_paired_1.fastq"
FASTQ_FILE_2     = FASTQS + "{sample}_paired_2.fastq"
UNPAIRED_FASTQ_1 = FASTQS + "{sample}_unpaired_1.fastq"
UNPAIRED_FASTQ_2 = FASTQS + "{sample}_unpaired_2.fastq"

MAPPING_DIR = "ALIGNMENT/"
makedirs(ABSOLUTE_PATH + MAPPING_DIR)
SAI_FILE_1     = MAPPING_DIR + "{sample}_1.sai"
SAI_FILE_2     = MAPPING_DIR + "{sample}_2.sai"
SAM_FILE       = MAPPING_DIR + "{sample}.sam"
BAM_FILE       = MAPPING_DIR + "{sample}.bam"

INDEXED_BAM    = MAPPING_DIR + "{sample}.bai"
MARKED_FILE    = MAPPING_DIR + "{sample}.noDup.bam"
REALIGNED_FILE = MAPPING_DIR + "{sample}.noDup.realigned.bam"

PURIFICATION_DIR = "PURIFICATION/"
makedirs(ABSOLUTE_PATH + PURIFICATION_DIR)
PCR_METRICS                = PURIFICATION_DIR + "{sample}.pcr_metrics"
INTERVAL_FILE              = PURIFICATION_DIR + "{sample}.indels.intervals"
RECAL_FILE                 = PURIFICATION_DIR + "{sample}.recalSpots.grp"
RECALIBRATED_BAM           = PURIFICATION_DIR + "{sample}.noDup.realigned.recalibrated.bam"
VCF_FILE                   = PURIFICATION_DIR + "{sample}.vcf"
SNP_METRICS                = PURIFICATION_DIR + "{sample}.snp.metrics"
VCF_FILE_NO_SNPS           = PURIFICATION_DIR + "{sample}.noSNPs.vcf"
NO_READ_EDGES              = PURIFICATION_DIR + "{sample}.noReadEdges.vcf"
NON_REPEATS_VCF            = PURIFICATION_DIR + "{sample}.non_repeats.vcf"
NO_SPLICE_JUNCTION         = PURIFICATION_DIR + "{sample}.noSpliceJunction.vcf"
NO_HOMOPOLYMERS_VCF        = PURIFICATION_DIR + "{sample}.noHomo.vcf"
FASTA_OF_READS_WITH_SNPS   = PURIFICATION_DIR + "{sample}.reads_with_snps.fasta"
BLAT_PSL_FILE              = PURIFICATION_DIR + "{sample}.psl"
REPEATS_MISMATCHES_VCF     = PURIFICATION_DIR + "{sample}.repeats.mismatches.vcf" 
NONREPEAT_MISMATCHES_VCF   = PURIFICATION_DIR + "{sample}.non_repeat.mismatches.vcf"
REPEATS_EDITING_ONLY_VCF   = PURIFICATION_DIR + "{sample}.repeat.editing_sites.vcf"
NONREPEAT_EDITING_ONLY_VCF = PURIFICATION_DIR + "{sample}.non_repeat.editing_sites.vcf"
COMBINED_EDITING_SITES     = PURIFICATION_DIR + "{sample}.all_editing_sites.vcf"


samples = []
for line in open(config["samples"]):
    samples.append(line.strip())

rule all: 
    input 
        expand(COMBINED_EDITING_SITES, sample=samples)


rule fastqdump:
    input: SRA_FILE
    output:
        fastq_1 = temp(FASTQ_1),
        fastq_2 = temp(FASTQ_2)
    params: fastq_dir = FASTQ_DIR
    threads: 8
    priority: 1
    shell: """fastq-dump --split-3 --O {params.fastq_dir} %s/{input}; touch -c {output.fastq_1}; touch -c {output.fastq_2};""" % ABSOLUTE_PATH


rule trim_fastqs:
    input:
        fastq_1 = FASTQ_1,
        fastq_2 = FASTQ_2
    output:
        trimmed_fastq_paired_1   = FASTQ_FILE_1,
        trimmed_fastq_paired_2   = FASTQ_FILE_2,
        trimmed_fastq_unpaired_1 = UNPAIRED_FASTQ_1,
        trimmed_fastq_unpaired_2 = UNPAIRED_FASTQ_2,
    priority: 2
    threads: 8
    shell: """
    module load java-1.8.0_40
    java -jar /scratch/global/tmweir01/bin/trimmomatic-0.36.jar PE -phred33 {input.fastq_1} {input.fastq_2}      \
                                               {output.trimmed_fastq_paired_1} {output.trimmed_fastq_unpaired_1} \
                                               {output.trimmed_fastq_paired_2} {output.trimmed_fastq_unpaired_2} \
                                               ILLUMINACLIP:all_fastq_primers.fa:2:30:10                         \
                                               LEADING:3 TRAILING:3 SLIDINGWINDOW:4:15 MINLEN:36
    """


rule align_fastq1_to_genome: 
    input:  FASTQ_FILE_1
    output: temp(SAI_FILE_1)
    params:
        max_diff = MAX_DIFF,
        seed_diff = SEED_DIFF,
        reference_genome = REFERENCE_GENOME
    threads: THREADS
    shell: """bwa aln -t {threads} -n {params.max_diff} -k {params.seed_diff} {params.reference_genome} {input} > {output} """


rule align_fastq2_to_genome: 
    input:  FASTQ_FILE_2
    params:
        max_diff  = MAX_DIFF,
        seed_diff = SEED_DIFF,
        reference_genome = REFERENCE_GENOME
    threads: THREADS
    output: temp(SAI_FILE_2)
    shell: """bwa aln -t {threads} -n {params.max_diff} -k {params.seed_diff} {params.reference_genome} {input} > {output} """


rule convert_sai_to_sam:
    input: 
        sai_1 = SAI_FILE_1,
        sai_2 = SAI_FILE_2,  
        fastq_1 = FASTQ_FILE_1,
        fastq_2 = FASTQ_FILE_2
    params:
        read_group_header = "@RG\\tID:bwa\\tSM:A\\tPL:ILLUMINA\\tPU:HiSEQ2000",
        reference_genome = REFERENCE_GENOME    
    output: temp(SAM_FILE)
    threads: THREADS
    shell: """bwa sampe -r "{params.read_group_header}" -f {output} {params.reference_genome} {input.sai_1} {input.sai_2} {input.fastq_1} {input.fastq_2} """


rule sam_to_bam:
    input: SAM_FILE
    output: temp(BAM_FILE)
    threads: THREADS
    shadow: "shallow"
    shell: """samtools sort -@ {threads} -o {output} {input} """


rule index_bam:
    input:  BAM_FILE
    threads: THREADS
    output: temp(INDEXED_BAM)
    shell: """samtools index {input} {output}"""


rule mark_PCR_duplicates: 
    input: BAM_FILE 
    params: 
        picard_tools = PICARD_PATH,
        tmp_path = TMP_PATH
    threads: THREADS
    output: 
         marked_file = MARKED_FILE, 
         pcr_metrics = PCR_METRICS,
    shell: """
    module load java-1.8.0_40
    java -Xmx32G -Djava.io.tmpdir={params.tmp_path} -jar {params.picard_tools} INPUT={input} OUTPUT={output.marked_file} METRICS_FILE={output.pcr_metrics} VALIDATION_STRINGENCY=LENIENT CREATE_INDEX=true TMP_DIR={params.tmp_path}
    """


rule identify_regions_for_realignment:
    input: MARKED_FILE
    params:
        gatk             = GATK_PATH,
        reference_genome = REFERENCE_GENOME,
        tmp_path         = TMP_PATH
    threads: THREADS 
    output: INTERVAL_FILE 
    threads: THREADS
    shell: """
    module load java-1.8.0_40
    java -Xmx32G -Djava.io.tmpdir={params.tmp_path} -jar {params.gatk} -nt {threads} -T RealignerTargetCreator -R {params.reference_genome} -I {input} -o {output} -l ERROR """   


rule perform_realignment:
    input: 
        marked_file = MARKED_FILE,
        interval_file = INTERVAL_FILE
    params: 
        gatk             = GATK_PATH, 
        reference_genome = REFERENCE_GENOME,
        tmp_path         = TMP_PATH
    threads: THREADS
    output: temp(REALIGNED_FILE)
    shell: """
    module load java-1.8.0_40
    java -Xmx32G -Djava.io.tmpdir={params.tmp_path} \
         -jar {params.gatk}                         \
         -T IndelRealigner                          \
         -R {params.reference_genome}               \
         -I {input.marked_file}                     \
         -l ERROR                                   \
         -targetIntervals {input.interval_file}     \
         -o {output}
    """


rule find_quality_score_recalibration_spots:
    input: 
        realigned_file = REALIGNED_FILE
    output: 
        recalibrated_file = RECAL_FILE
    threads: THREADS
    params: 
        gatk = GATK_PATH, 
        reference_genome = REFERENCE_GENOME,
        snps = SNPS
    shell: """
    module load java-1.8.0_40
    java -Xmx32G -jar {params.gatk}    \
        -T BaseRecalibrator            \
        -l ERROR                       \
        -R {params.reference_genome}   \
        -knownSites {params.snps}      \
        -I {input.realigned_file}      \
        -cov CycleCovariate            \
        -cov ContextCovariate          \
        -o {output.recalibrated_file}                         
    """


rule quality_score_recalibration:
    input: 
        realigned_file = REALIGNED_FILE,
        recalibrated_file = RECAL_FILE,
    output: RECALIBRATED_BAM 
    threads: THREADS
    params: 
        gatk = GATK_PATH, 
        reference_genome = REFERENCE_GENOME
    shell: """
    module load java-1.8.0_40
    java -Xmx32G -jar {params.gatk}     \
        -T PrintReads                   \
        -l ERROR                        \
        -R {params.reference_genome}    \
        -I {input.realigned_file}       \
        -BQSR {input.recalibrated_file} \
        -o {output}
    """


rule unified_genotyper:
    input: RECALIBRATED_BAM
    output: 
        snp_metrics = SNP_METRICS, 
        vcf         = VCF_FILE
    threads: THREADS
    params:
        gatk = GATK_PATH, 
        reference_genome = REFERENCE_GENOME,
        snps = SNPS,
        threads = THREADS,
        stand_call = STAND_CALL,
        stand_emit = STAND_EMIT,
    shell: """
    module load java-1.8.0_40
    java -Xmx32G -jar {params.gatk}                                           \
         -T UnifiedGenotyper -R {params.reference_genome} -glm SNP -I {input} \
         -D {params.snps} -o {output.vcf} -metrics {output.snp_metrics}       \
         -nt {params.threads} -l ERROR -stand_call_conf {params.stand_call}   \
         -A Coverage -A AlleleBalance -A BaseCounts
    """


rule remove_snps:
    input: VCF_FILE
    threads: 8
    output: VCF_FILE_NO_SNPS
    shell: """python3 src/remove_annotated_snps_and_below_threshold.py {input} > {output} """
  

rule remove_edge_mismatches:
    input: 
        vcf = VCF_FILE_NO_SNPS,
        bam = RECALIBRATED_BAM
    threads: 8
    params: 
        min_distance = 3
    output: NO_READ_EDGES
    shell: """python3 src/remove_edge_mismatches.py --vcf {input.vcf} --bam {input.bam} --min_distance {params.min_distance} > {output} """


# -----------------------------------------------------
# Be sure to covert one-based coordinate VCF files to zero-based BED files. 
rule get_repeat_region_snps: 
    input: NO_READ_EDGES
    params: repeats_bed = REPEATS
    output: REPEATS_MISMATCHES_VCF
    shell:  
        """python3 src/remove_title.py {input} | vectools slice --keep-cols 0,1,1: - | vectools subtract -r --only-apply-on 0 --scalar 1 - | bedtools intersect -wa -a stdin -b {params.repeats_bed} | vectools slice --remove-cols 2 - > {output} """ 


rule remove_non_editing_snps: 
    input:  REPEATS_MISMATCHES_VCF
    threads: 8
    params: editing_types = config["editing_types"]
    output: REPEATS_EDITING_ONLY_VCF
    shell: """ vectools vrep --only-apply-on 3,4 {params.editing_types} {input} > {output} """ 


# The only difference is  -v
# Be sure to covert one-based coordinate VCF files to zero-based BED files. 
rule get_non_alu_regions: 
    input: NO_READ_EDGES
    threads: 8
    params: repeats_bed = REPEATS
    output: NON_REPEATS_VCF
    shell: 
        """python3 src/remove_title.py {input} | vectools slice --keep-cols 0,1,1: - | vectools subtract -r --only-apply-on 0 --scalar 1 - | bedtools intersect -v -wa -a stdin -b {params.repeats_bed} | vectools slice --remove-cols 1 - > {output} """ 


# Erase variants from intronic splice junctions.
rule remove_intron_splice_junctions: 
    input:  NON_REPEATS_VCF
    threads: 8
    params: splice_junctions = SPLICE_JUNCTIONS
    output: NO_SPLICE_JUNCTION
    shell: """python3 src/remove_title.py {input} | vectools slice --keep-cols 0,1,1: - | vectools subtract -r --only-apply-on 0 --scalar 1 - | bedtools intersect -v -wa -a stdin -b {params.splice_junctions} | vectools slice --remove-cols 1 - > {output} """


# Erase variants from homopolymers
rule remove_homopolymers: 
    input:  NO_SPLICE_JUNCTION
    threads: THREADS
    params: 
        reference_genome = REFERENCE_GENOME,
        distance = DISTANCE,
    output: NO_HOMOPOLYMERS_VCF
    shell: """python3 src/remove_homopolymers.py --vcf {input} --reference-genome {params.reference_genome} --distance {params.distance} > {output} """


rule generate_snp_overlap_fasta:
    input:
        vcf = NO_HOMOPOLYMERS_VCF,
        bam = RECALIBRATED_BAM
    threads: 8
    output: FASTA_OF_READS_WITH_SNPS
    shell: """python3 src/generate_fasta_from_vcf_and_bam.py --vcf {input.vcf} --bam {input.bam} > {output} """


rule run_blat:
    input: FASTA_OF_READS_WITH_SNPS
    threads: 8
    params: ref_genome = REFERENCE_GENOME,
    output: BLAT_PSL_FILE
    shell: """blat -stepSize=5 -repMatch=2253 -minScore=20 -minIdentity=0 -noHead {params.ref_genome} {input} {output} """


rule filter_psl: 
    input: 
        vcf = NO_HOMOPOLYMERS_VCF, 
        psl = BLAT_PSL_FILE
    threads: 8
    output: NONREPEAT_MISMATCHES_VCF
    shell: """python3 src/filter_psl_file.py --vcf {input.vcf} --psl {input.psl} > {output}"""


rule remove_non_editing_snps_nonmismatches: 
    input:  NONREPEAT_MISMATCHES_VCF
    threads: 8
    params: editing_types = config["editing_types"]
    output: NONREPEAT_EDITING_ONLY_VCF
    shell: """ vectools vrep --only-apply-on 3,4 {params.editing_types} {input} > {output} """ 

 
# Recombine ALU and non-ALU
rule combine_filtered_editing_sites: 
    input: 
        repeats    = REPEATS_EDITING_ONLY_VCF,
        non_repeat = NONREPEAT_EDITING_ONLY_VCF
    threads: 8
    output: COMBINED_EDITING_SITES 
    shell: """cat {input.repeats} {input.non_repeat} > {output} """
        

