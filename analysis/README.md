The Snakemake pipelines included here can be used to generate the results used in the manuscript "DRETools: A tool-suite for differential RNA editing detection". Additionally, the intermediate files generated by the analyses are also included. 

Unfortunately, the alignment files are to large to include, but they can be generated by downloading the SRA files and executing the run_pipelines.sh script.  Ensembl version 90 annotations were used to generate the alignment files. See http://rnaeditor.uni-frankfurt.de/ for information for instructions on preparing reference data.




| SRA_ID     | File_Name                                   | Sample_Source | File_Size | IPFS_Hash                                      |
|------------|---------------------------------------------|---------------|-----------|------------------------------------------------|
| SRR3091828 | SRR3091828.noDup.realigned.recalibrated.bam | HUVEC siADAR1 | 3.2G      | QmRNbcRoFpPHrB6XCH2HbHAGU8iG2ZUz7m6PFRtfRsZ9kR |
| SRR3091829 | SRR3091829.noDup.realigned.recalibrated.bam | HUVEC siADAR1 | 5.4G      | Qmc4oTxW9dHSR8nwccGxheAkxk45DdoUBRSZmvVTLbSpm6 |
| SRR3091830 | SRR3091830.noDup.realigned.recalibrated.bam | HUVEC siADAR1 | 6.6G      | QmT2qMwYhy9kis98F91UjbdDYm3erVF2LP1fTJbZm1Aesr |
| SRR3091831 | SRR3091831.noDup.realigned.recalibrated.bam | HUVEC scrRNA  | 3.8G      | QmW9vAWq9Dfuv66WiPZES7fpH6TmMa2vwMDDdKeqkReUAq |
| SRR3091832 | SRR3091832.noDup.realigned.recalibrated.bam | HUVEC scrRNA  | 6.2G      | QmNnQ8c5Jg3DoffV5vCiNFDm8U3YjWjSKBxyRYkmFfGzoN |
| SRR3091833 | SRR3091833.noDup.realigned.recalibrated.bam | HUVEC scrRNA  | 4.8G      | QmXf72kmQCXSbwyGMSAb3W5hMDHMSBmbwDGdCR2Eft9V7A |
| SRR3192408 | SRR3192408.noDup.realigned.recalibrated.bam | K562          | 15G       | QmTvu7eG2GrXj2ggLeEi9Pp4EUEubTMyxJiEMZjwemxcft |
| SRR3192409 | SRR3192409.noDup.realigned.recalibrated.bam | K562          | 19G       | QmaTvDhUSvHDkj3Ja9pgX7dA2gwFx733HMYcaN396s1GRV |
| SRR3192410 | SRR3192410.noDup.realigned.recalibrated.bam | K562          | 17G       | Qmc7CoR3gjevsiEyDUYFNYV7QxCGxetVNqVbR678EP3Pj1 |
| SRR3192411 | SRR3192411.noDup.realigned.recalibrated.bam | K562          | 13G       | QmRSsBCDjuwhNterCkDXvgdNyurvQWNH8XpkYC61VViUSd |
| SRR3192412 | SRR3192412.noDup.realigned.recalibrated.bam | K562          | 14G       | QmczrwYvAmGE9DfnbX8qjo81QpzGbJKyeke8Trdt31HmQn |
| SRR3192413 | SRR3192413.noDup.realigned.recalibrated.bam | K562          | 13G       | Qmc5sZnJXapTCmA3VorNQTaKhgdDhNDSBXbmrNma2hBcV3 |
| SRR3192414 | SRR3192414.noDup.realigned.recalibrated.bam | K562          | 9.3G      | QmRp4rGZFYz13ZG9mxgbbeNPhPTdTdwTreCsEUaYv15aVf |
| SRR3192415 | SRR3192415.noDup.realigned.recalibrated.bam | K562          | 13G       | Qme7FRWCMnAtV9dg5dQZbik2yZMnkZsF7EVs47fXdABZQX |
| SRR3192416 | SRR3192416.noDup.realigned.recalibrated.bam | K562          | 24G       | QmW68HBMR8pZpj7ErhxwufvfLC77w5rEQcAXhuD6sebNh5 |
| SRR3192417 | SRR3192417.noDup.realigned.recalibrated.bam | K562          | 23G       | QmYeeg9oEYZu41bXgjeSMBMbRcaq6edtraQ7DozYhNjeYw |
| SRR3192422 | SRR3192422.noDup.realigned.recalibrated.bam | K562          | 22G       | QmbwuzfVZnK3zPMRHUoWpqhgdgLGdg7fzZ2CMiBAFRrJvf |
| SRR3192423 | SRR3192423.noDup.realigned.recalibrated.bam | K562          | 24G       | QmVxVw6mAbr3zeLVVGqSk2u8jVvm2ekcKrqYJVZuX5HPuy |
| SRR3192544 | SRR3192544.noDup.realigned.recalibrated.bam | K562          | 16G       | QmPmWqs3Fc1KP1ikuCAobLLip7DFxSUNqMdLxb5pJHoqxE |
| SRR3192545 | SRR3192545.noDup.realigned.recalibrated.bam | K562          | 15G       | QmUk4vHHvrr92F9J5gt2i24NoBD5gTbLB8wBmMPDNSPUKk |
| SRR3192396 | SRR3192396.noDup.realigned.recalibrated.bam | GM12787       | 19G       | QmYB1PPcNHU67MBELUuDmhj7vFn4S6TNmXypQYSVM5mTvX |
| SRR3192397 | SRR3192397.noDup.realigned.recalibrated.bam | GM12787       | 17G       | QmdyaKbkcaFWhxTu52vo9kMKFswK3mrEaBFfA7DyEXUYJF |
| SRR3192398 | SRR3192398.noDup.realigned.recalibrated.bam | GM12787       | 15G       | QmTte3sKeUiMWriZArMV3vP8arRGZXHUKcRoQ8iWdCttLT |
| SRR3192399 | SRR3192399.noDup.realigned.recalibrated.bam | GM12787       | 17G       | QmYNUBGturPJCbdY7V2AtoFHYGq1gBLd7b428tsbVfe4Uh |
| SRR3192400 | SRR3192400.noDup.realigned.recalibrated.bam | GM12787       | 22G       | QmT4fuiWGWfRQFc4RxXRKyjeWZF2y4j1GqFUYMd7334Z31 |
| SRR3192401 | SRR3192401.noDup.realigned.recalibrated.bam | GM12787       | 23G       | QmZBEecM5zFRQ93BLSd6T8AAJT8D3h9WR9Hnw3q2Ywiu87 |
| SRR3192402 | SRR3192402.noDup.realigned.recalibrated.bam | GM12787       | 11G       | QmV2iJa1o6DqE3mx5YWuvM6kD3mXgGAG65Dgx45BxdqCZ7 |
| SRR3192403 | SRR3192403.noDup.realigned.recalibrated.bam | GM12787       | 28G       | QmetkQxE5B7p1DtU7tYo4n8wRyf2UHSEUgE3XocJom9gC2 |
| SRR3192406 | SRR3192406.noDup.realigned.recalibrated.bam | GM12787       | 21G       | QmT3uc8ikybJtAKifgenfv6AKpefeyVpxLpAC4HEgYDAhS |
| SRR3192407 | SRR3192407.noDup.realigned.recalibrated.bam | GM12787       | 29G       | QmbvNBugZsjT7LnE6f564SUpXL2QVjCW6kGZLGCc19YLb5 |
| SRR3192657 | SRR3192657.noDup.realigned.recalibrated.bam | GM12787       | 16G       | QmXF1pjrtaWzJJEii7hW96JbnWcndTboLY8YDRJUd3tVA6 |
| SRR3192658 | SRR3192658.noDup.realigned.recalibrated.bam | GM12787       | 17G       | QmNQFkm9jZPixbMtPrACGgkxwzSCU6LbRG7RNAstNRawVs |
| SRR5048071 | SRR5048071.noDup.realigned.recalibrated.bam | GM12787       | 21G       | QmVikhCLJ7LbSfUvHAoPZBs3YnN6xJF4R9TAKcXp5tQc49 |
| SRR5048072 | SRR5048072.noDup.realigned.recalibrated.bam | GM12787       | 21G       | QmfTxbX4gfchuJ8np1pSWnBgvqYT39ZaYy2gk2bra1buWt |



